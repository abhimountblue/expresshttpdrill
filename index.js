const express = require('express')
const app = express()
const fs = require('fs')
const { v4: uuidv4 } = require('uuid')
require('dotenv').config()

app.get('/logs', (request, response) => {
    fs.readFile('logs.log', 'utf-8', (err, data) => {
        if (err) {
            response.setHeader('Content-Type', 'text/html')
            response.status(500).send('<h1>Internal Server Error</h1>')
        } else {
            response.setHeader('Content-Type', 'text/plain')
            response.status(200).send(data)
        }
    })
})

app.use((request, response, next) => {
    const appendData = `Time : ${new Date()} ,Method : ${request.method} ,url : ${request.url}\n`
    fs.appendFile('logs.log', appendData, (err) => {
        if (err) {
            response.setHeader('Content-Type', 'text/html')
            response.status(500).send('<h1>Internal Server Error</h1>')
        } else {
            next()
        }
    })
})

app.get('/html', (request, response) => {
    fs.readFile('htmlProblem.html', 'utf-8', (err, data) => {
        if (err) {
            response.setHeader('Content-Type', 'text/html')
            response.status(500).send('<h1>Internal Server Error</h1>')
        } else {
            response.setHeader('Content-Type', 'text/html')
            response.status(200).send(data)
        }
    })
})

app.get('/json', (request, response) => {
    fs.readFile('jsonProblem.json', 'utf-8', (err, data) => {
        if (err) {
            response.setHeader('Content-Type', 'application/json')
            response.status(500).send('<h1>Internal Server Error</h1>')
        } else {
            response.setHeader('Content-Type', 'application/json')
            response.status(200).send(data)
        }
    })
})

app.get('/uuid', (request, response) => {
    response.setHeader('Content-Type', 'application/json')
    response.status(200).send({ 'uuid': uuidv4() })
})

app.get('/status/:id', (request, response) => {
    const statusCode = parseInt(request.params.id)
    if (typeof statusCode === 'number') {
        if (statusCode > 599 || statusCode < 100) {
            response.setHeader('Content-Type', 'text/html')
            response.status(500).send('<h1>Internal Server Error</h1>\n <h3>Status code range Error</h3>')
        } else {
            if (statusCode > 99 && statusCode < 200) {
                response.setHeader('Content-Type', 'text/html')
                response.status(200).send(`<h1>Status Code : ${statusCode}</h1>`);
            } else {
                response.setHeader('Content-Type', 'text/html')
                response.status(statusCode).send(`<h1>Status Code : ${statusCode}</h1>`)
            }
        }
    } else {
        response.setHeader('Content-Type', 'text/html')
        response.status(500).send('<h1>Internal Server Error</h1>\n <h3>Status code not a number</h3>')
    }
})

app.get('/delay/:time', (request, response) => {
    const time = request.params.time
    setTimeout(() => {
        response.setHeader('Content-Type', 'text/html')
        response.status(200).send(`<h1> Successful response after ${time} seconds</h1>`)
    }, time * 1000);
})

const PORT = process.env.PORT || 3000
app.listen(PORT, () => {
    console.log(`server is runing on port no ${PORT}`)
})



